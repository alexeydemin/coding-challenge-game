<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;
    private array $questions;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
        $this->questions = $this->getQuestions();

    }

    private function getQuestions(): array
    {
        $jsonObjects = $this->json->decodeFile(self::QUESTIONS_PATH);
        $questions = [];
        foreach ($jsonObjects as $object) {
            $questions[] = $this->jsonMapper->map($object, new Question());
        }

        return $questions;
    }

    public function getRandomQuestions(int $count = 5): array
    {
        shuffle( $this->questions);
        return array_slice($this->questions, 0, $count);
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $question = $this->getQuestion($id);

        return $question->getCorrectAnswer() == $answer ? $question->getPoints() : 0;
    }

    public function getQuestion(int $id): Question
    {
        foreach($this->questions as $question){
            if($question->getId() == $id){
                return $question;
            }
        }
    }
}
