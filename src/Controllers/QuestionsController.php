<?php


namespace Ucc\Controllers;


use Ucc\Services\QuestionService;
use Ucc\Session;
use Ucc\Http\JsonResponseTrait;

class QuestionsController extends Controller
{
    use JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;

        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);

        $question = $this->questionService->getRandomQuestions(1)[0];

        return $this->json(['question' => $question], 201);
    }

    public function answerQuestion(int $id): bool
    {
        $name = Session::get('name');
        $points = Session::get('points');
        $questionCount = (int)Session::get('questionCount');

        if ($name === null) {
            return $this->json('You must first begin a game', 400);
        }

        if ($questionCount > 4) {
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        $lastPoints = $this->questionService->getPointsForAnswer($id, $answer);
        $correctness = $lastPoints ? 'correct' : 'incorrect';
        $points += $lastPoints;
        Session::set('points', $points);
        Session::set('questionCount', $questionCount + 1);
        $message = "Your answer is {$correctness}. Current score is {$points}.";
        $question = $this->questionService->getRandomQuestions(1)[0];

        return $this->json(['message' => $message, 'question' => $question]);
    }
}